from modeling import registry
from .vgg import VGG
from .mobilenet import MobileNetV2
from .efficient_net import EfficientNet
from .fassd import FASSD
from .assd import ASSD
from .fssd import FSSD
__all__ = ['build_backbone', 'VGG', 'MobileNetV2', 'EfficientNet', 'FASSD', 'ASSD', 'FSSD']


def build_backbone(cfg):
    return registry.BACKBONES[cfg.MODEL.BACKBONE.NAME](cfg, cfg.MODEL.BACKBONE.PRETRAINED)
