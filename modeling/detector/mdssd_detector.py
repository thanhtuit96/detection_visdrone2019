from modeling.box_head.loss import MultiBoxLoss
import torch

from torch import nn
from layers import AttentionBlock, L2Norm, FusionBlock
import torch.nn.functional as F

class MDSSDDetector(nn.Module):
    def __init__(self, cfg):
        super(MDSSDDetector, self).__init__()
		# Attention
        self.attn1 = AttentionBlock(3)
		# model
        self.base = self.VGG16()
        self.norm4 = L2Norm(512, 20) # 38
        self.conv5_1 = nn.Conv2d(512, 512, kernel_size=3, padding=1, dilation=1)
        self.conv5_2 = nn.Conv2d(512, 512, kernel_size=3, padding=1, dilation=1)
        self.conv5_3 = nn.Conv2d(512, 512, kernel_size=3, padding=1, dilation=1)
        self.bn5 = nn.BatchNorm2d(512)	
        self.conv6 = nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6)
        self.bn6 = nn.BatchNorm2d(1024)

    def forward(self, images, targets=None):
        features = self.backbone(images)
        detections, detector_losses = self.box_head(features, targets)
        if self.training:
            return detector_losses
        return detections
