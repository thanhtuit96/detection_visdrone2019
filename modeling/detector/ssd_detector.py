from modeling.neck import build_neck
from torch import nn

from modeling.backbone import build_backbone
from modeling.box_head import build_box_head


class SSDDetector(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.backbone = build_backbone(cfg)
        self.neck = build_neck(cfg) # build neck
        self.box_head = build_box_head(cfg)

    def forward(self, images, targets=None):
        features = self.backbone(images)
        if self.neck:
            features = self.neck(features)
        detections, detector_losses = self.box_head(features, targets)
        if self.training:
            return detector_losses
        return detections