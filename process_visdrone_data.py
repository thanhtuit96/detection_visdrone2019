import cv2
import os
import numpy as np
import sys

import os
from pathlib import Path
from ultralytics.utils.downloads import download

urls = ['https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-train.zip',
        'https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-val.zip',
        'https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-test-dev.zip',
        'https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-test-challenge.zip']

label_dict = {
	"1" : "Pedestrian",
	"2" : "People",
	"3" : "Bicycle",
	"4" : "Car",
	"5" : "Van",
	"6" : "Truck",
	"7" : "Tricycle",
	"8" : "Awning-tricycle",
	"9" : "Bus",
	"10" : "Motor",
}

def object_string(label, bbox):
	req_str = '''
	<object>
		<name>{}</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>{}</xmin>
			<ymin>{}</ymin>
			<xmax>{}</xmax>
			<ymax>{}</ymax>
		</bndbox>
	</object>
	'''.format(label, bbox[0], bbox[1], bbox[2], bbox[3])
	return req_str


def visdrone2voc(dir):
    from PIL import Image
    from tqdm import tqdm
    def convert_box(size, box):
        # Convert VisDrone box to YOLO xywh box
        dw = 1. / size[0]
        dh = 1. / size[1]
        return (box[0] + box[2] / 2) * dw, (box[1] + box[3] / 2) * dh, box[2] * dw, box[3] * dh
    (dir / 'labels').mkdir(parents=True, exist_ok=True)  # make labels directory
    pbar = tqdm((dir / 'annotations').glob('*.txt'), desc=f'Converting {dir}')
    for f in pbar:
        img_size = Image.open((dir / 'images' / f.name).with_suffix('.jpg')).size
        lines = []
        with open(f, 'r') as file:  # read annotation.txt
            for row in [x.split(',') for x in file.read().strip().splitlines()]:
                if row[4] == '0':  # VisDrone 'ignored regions' class 0
                    continue
                cls = int(row[5])
                box = convert_box(img_size, tuple(map(int, row[:4])))
                lines.append(f"{cls} {' '.join(f'{x:.6f}' for x in box)}\n")
                with open(str(f).replace(f'{os.sep}annotations{os.sep}', f'{os.sep}labels{os.sep}'), 'w') as fl:
                    fl.writelines(lines)  # write label.txt

# Convert
for d in 'VisDrone2019-DET-train', 'VisDrone2019-DET-val':
    visdrone2voc(dir / d)  # convert VisDrone annotations to YOLO labels

target = sys.argv[1]
input_img_folder = 'VisDrone2019-DET-%s/images' % target
input_ann_folder = 'VisDrone2019-DET-%s/annotations' % target
DATA_DIR = "datasets"
output_ann_folder = '%s/DET2019/Annotations' % DATA_DIR
output_img_folder = '%s/DET2019/JPEGImages' % DATA_DIR
output_img_sets = '%s/DET2019/ImageSets' % DATA_DIR

os.makedirs(output_img_folder, exist_ok=True)
os.makedirs(output_ann_folder, exist_ok=True)
os.makedirs(output_img_sets, exist_ok=True)


image_list = os.listdir(input_img_folder)
annotation_list = os.listdir(input_ann_folder)
img_sets = open(output_img_sets + "/%s.txt" % target, 'w')

label_dict = {
	"1" : "Pedestrian",
	"2" : "People",
	"3" : "Bicycle",
	"4" : "Car",
	"5" : "Van",
	"6" : "Truck",
	"7" : "Tricycle",
	"8" : "Awning-tricycle",
	"9" : "Bus",
	"10" : "Motor",
}

thickness = 2
color = (255,0,0)
count = 0

def object_string(label, bbox):
	req_str = '''
	<object>
		<name>{}</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>{}</xmin>
			<ymin>{}</ymin>
			<xmax>{}</xmax>
			<ymax>{}</ymax>
		</bndbox>
	</object>
	'''.format(label, bbox[0], bbox[1], bbox[2], bbox[3])
	return req_str

for annotation in annotation_list:
	annotation_path = os.path.join(os.getcwd(), input_ann_folder, annotation)
	xml_annotation = annotation.split('.txt')[0] + '.xml'
	xml_path = os.path.join(os.getcwd(), output_ann_folder, xml_annotation)
	img_file = annotation.split('.txt')[0] + '.jpg'
	img_path = os.path.join(os.getcwd(), input_img_folder, img_file)
	output_img_path = os.path.join(os.getcwd(), output_img_folder, img_file)
	img = cv2.imread(img_path)
	annotation_string_init = '''
<annotation>
	<folder>annotations</folder>
	<filename>{}</filename>
	<path>{}</path>
	<source>
		<database>Unknown</database>
	</source>
	<size>
		<width>{}</width>
		<height>{}</height>
		<depth>{}</depth>
	</size>
	<segmented>0</segmented>'''.format(img_file, img_path, img.shape[0], img.shape[1], img.shape[2])

	file = open(annotation_path, 'r')
	lines = file.readlines()
	for line in lines:
		new_line = line.strip('\n').split(',')
		if new_line[4] == '0':
			continue
		new_coords_min = (int(new_line[0]), int(new_line[1]))
		new_coords_max = (int(new_line[0])+int(new_line[2]), int(new_line[1])+int(new_line[3]))
		bbox = (int(new_line[0]), int(new_line[1]), int(new_line[0])+int(new_line[2]), int(new_line[1])+int(new_line[3]))
		label = label_dict.get(new_line[5])
		req_str = object_string(label, bbox)
		annotation_string_init = annotation_string_init + req_str
	cv2.imwrite(output_img_path, img)
	annotation_string_final = annotation_string_init + '</annotation>'
	f = open(xml_path, 'w')
	f.write(annotation_string_final)
	img_sets.write(annotation.split('.txt')[0] + "\n")
	f.close()
	count += 1
	print('[INFO] Completed {} image(s) and annotation(s) pair'.format(count))