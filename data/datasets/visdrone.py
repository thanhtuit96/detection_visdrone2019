import os
import torch.utils.data
import numpy as np
import xml.etree.ElementTree as ET
from PIL import Image

from structures.container import Container

class VisDroneDataset(torch.utils.data.Dataset):
    # 问题是datasets是在哪里进行设置的，应该会有一个参数，这就很麻烦
    # 也就是11个类别再加上background
    # 这边基本完事了，把文件加载进来就可以，修改一下类名， 然后看下主启动函数那边
    class_names = ('__background__','pedestrian', 'people', 'bicycle', 'car', 'van',
                   'truck', 'tricycle', 'awning-tricycle', 'bus', 'motor')
    def __init__(self, data_dir, split, transform=None, target_transform=None, keep_difficult=False):
        """Dataset for VOC data.
        """
        self.data_dir = data_dir
        self.split = split
        self.transform = transform
        self.target_transform = target_transform
        self.num_samples = 0
        self.ids = []
        self.boxes = []
        self.labels = []
        self.difficulties = []
        # images, annotations
        for i in os.listdir(os.path.join(data_dir,split,"annotations")):
          self.num_samples += 1
          self.ids.append(i)
          box = []
          labels = []
          difficulties = []
          with open(os.path.join(data_dir,split,"annotations", i)) as f:
            f = f.read().split("\n")
            f = f[:-1]
          num_objs = len(f)
          for j in range(num_objs):
            f[j] = f[j].split(",")
            xmin = float(f[j][0])
            ymin = float(f[j][1])
            w = float(f[j][2])
            h = float(f[j][3])
            if f[j][4] == '0': # ignore
              continue
            box.append([xmin,ymin,xmin+w,ymin+h])
            labels.append(int(f[j][5]))
            difficulties.append(0)

          self.boxes.append(np.array(box, dtype=np.float32))
          self.labels.append(np.array(labels, dtype=np.int64))
          self.difficulties.append(np.array(difficulties, dtype=np.uint8))

    def __getitem__(self, i):
      image_id = self.ids[i]
      boxes = self.boxes[i]
      labels = self.labels[i]
      image = Image.open(os.path.join(self.data_dir, self.split,"images" , image_id[:-4]+".jpg"), mode='r')
      image = image.convert('RGB')
      if self.transform:
        image, boxes, labels = self.transform(image, boxes, labels)
      if self.target_transform:
        boxes, labels = self.target_transform(boxes, labels)
      targets = Container(
        boxes=boxes,
        labels=labels,
      ) 
      return image, targets, i

    def __len__(self):
        return len(self.ids)

    def get_annotation(self, i):
      image_id = self.ids[i]
      boxes = self.boxes[i]
      labels = self.labels[i]
      difficulties = self.difficulties[i]
      return image_id, (boxes.numpy(), labels.numpy(), difficulties)
    
    def _read_image(self, image_id):
      image = Image.open(os.path.join(self.data_dir, self.split,"images" , image_id[:-4]+".jpg"), mode='r')
      image = image.convert('RGB')      
      image = np.array(image)
      return image
    
    def get_img_info(self, index):
      image_id = self.ids[index]
      image = Image.open(os.path.join(self.data_dir, self.split,"images" , image_id[:-4]+".jpg"), mode='r')
      h, w, c = image.shape
      return {"height": h, "width": w}
